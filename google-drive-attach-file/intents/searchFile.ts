import { FetchData, TOAUTH2AuthContext, TFetchDataCallback } from '@bearer/intents'
import Client from './client'

export default class SearchFileIntent {
    static intentType: any = FetchData


    static action(context: TOAUTH2AuthContext, params: any, body: any, callback: TFetchDataCallback) {
        const {phrase} = params
        Client(context.authAccess.accessToken).get('/files', {params: {q: `name contains '${phrase}'`}})
            .then(({data}) => {
                const files = data.files
                callback({data: files})
            })
            .catch(error => {
                callback({error: error.toString()})
            })
    }
}