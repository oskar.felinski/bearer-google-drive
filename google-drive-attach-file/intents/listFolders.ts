import {FetchData, TOAUTH2AuthContext, TFetchDataCallback} from '@bearer/intents'
import Client from './client'

export default class listFoldersIntent {
    static intentType: any = FetchData

    static action(context: TOAUTH2AuthContext, params: any, body: any, callback: TFetchDataCallback) {
        const {id} = params
        Client(context.authAccess.accessToken).get('/files', {params: {q: id ? `'${id}' in parents` : `'root' in parents`}})
            .then(({data}) => {
                const files = data.files
                callback({data: files})
            })
            .catch(error => {
                callback({error: error.toString()})
            })
    }
}