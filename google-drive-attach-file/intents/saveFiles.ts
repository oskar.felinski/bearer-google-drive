import { SaveState, TOAUTH2AuthContext, TSaveStateCallback } from '@bearer/intents'

export default class SavePullRequestsIntent {
    static intentType: any = SaveState

    static action(
        _context: TOAUTH2AuthContext,
        _params: any,
        body: any,
        state: any,
        callback: TSaveStateCallback
    ): void {
        callback({
            state: {
                ...state,
                body
            },
            data: body
        })
    }
}