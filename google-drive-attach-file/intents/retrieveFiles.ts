import { RetrieveState, TOAUTH2AuthContext, TRetrieveStateCallback } from '@bearer/intents'

import Client from './client'

export default class RetrieveFilesIntent {
  static intentName: string = 'retrieveFiles'
  static intentType: any = RetrieveState

  static action(context: TOAUTH2AuthContext, _params: any, state: any, callback: TRetrieveStateCallback) {
    Client(context.authAccess.accessToken).get(`/files/${state.body.files.id}`, {params: {fields: 'webContentLink, name, id, mimeType' }})
        .then(({ data }) => {
          callback({data})
        })
        .catch(error => {
          callback({ error: error.toString() })
        })
  }
}