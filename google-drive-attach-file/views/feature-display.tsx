import {RootComponent, Input} from '@bearer/core'
import '@bearer/ui'

@RootComponent({
    role: 'display',
    styleUrl: 'feature.css',
    group: 'feature'
})
export class FeatureDisplay {
    @Input() files: any


    render() {
        if (!this.files) return null
        const {name, webContentLink} = this.files
        const content = (
            <div>
                <img src="https://upload.wikimedia.org/wikipedia/commons/7/75/Google_Drive_Logo.svg" alt="icon"
                     class="drive-ico" style={{
                    width: "16px", position: "relative", top: "2px", "margin-right": "7px"
                }}/>
                <span>{name}</span>
            </div>

        )
        return <div><a href={webContentLink}>
            <bearer-button kind="light" content={content}/>
        </a> <i onClick={() => {this.files = undefined}} class="material-icons">
            cancel
        </i></div>
    }
}
