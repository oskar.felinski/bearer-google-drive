import {RootComponent, Prop, Output, State} from '@bearer/core'

// import '@bearer/ui'

@RootComponent({
    role: 'action',
    styleUrl: 'feature.css',
    group: 'feature'
})
export class FeatureAction {
    @Output() files: any
    @State() modalOpened: boolean = false

    getSelectedFile = (data): void => {
        if (data) {
            this.files = data
        }
    }
    start = () => this.modalOpened = !this.modalOpened
    closeModal = () => this.modalOpened = false

    render() {
        const content = (<div>
            <img src="https://upload.wikimedia.org/wikipedia/commons/7/75/Google_Drive_Logo.svg" alt="icon"
                 class="drive-ico" style={{
                width: "16px", position: "relative", top: "2px", "margin-right": "7px"
            }}/>
            <span>Attach a file</span>
        </div>)

        return (
            <div>
                <bearer-button kind="light" content={content} onClick={this.start}/>
                {this.modalOpened ? <popin-modal closeModal={this.closeModal}>
                    <bearer-authorized
                        renderAuthorized={() =>
                            <list-folders getSelectedFile={this.getSelectedFile} closeModal={this.closeModal}/>
                        }
                        renderUnauthorized={({authenticate}) =>
                            <bearer-button kind="light" content="Authenticate"
                                           onClick={() => authenticate()}/>
                        }
                    />
                </popin-modal> : null}
            </div>
        )
    }
}