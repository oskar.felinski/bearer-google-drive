import {Component, Prop} from '@bearer/core'

@Component({
    tag: 'list-item',
    styleUrl: 'listItem.css',
    shadow: false
})
export class ListItem {
    @Prop() file
    @Prop() selectId
    @Prop() selectFolder

    render() {
        const {name, mimeType, id} = this.file
        return <div class="list-item"><span class="file-name">{name}</span>
            {mimeType !== 'application/vnd.google-apps.folder' ?
                <label class="container" onClick={() => this.selectId(this.file)}>
                    <input type="radio" name="file" value={id}/>
                    <span class="checkmark"></span>
                </label> : <i onClick={() => this.selectFolder(id)} class="material-icons">arrow_forward_ios</i>}
        </div>
    }
}