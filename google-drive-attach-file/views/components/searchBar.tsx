import {Component, Prop, State} from '@bearer/core'

@Component({
    tag: 'search-bar',
    styleUrl: 'searchBar.css',
    shadow: false
})
export class SearchBar {
    @Prop() search
    @State() searchPhrase = ''

    onChangeValue = (event) => {
        this.searchPhrase = event.target.value
    }
    handleSubmit = (event) => {
        event.preventDefault()
        this.search(this.searchPhrase)
    }

    render() {
        return <div>
            <form class="search-bar" onSubmit={this.handleSubmit}>
                <button type="submit"><i class="material-icons">search</i></button>
                <input type="text" id="search" placeholder="Search file" name="search" value={this.searchPhrase}
                       onInput={this.onChangeValue}/>
            </form>
        </div>
    }
}