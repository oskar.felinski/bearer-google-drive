import {BearerFetch, Component, Intent, Prop, State} from '@bearer/core'

@Component({
    tag: 'list-folders',
    styleUrl: 'listItem.css',
    shadow: true
})
export class ListFolders {
    @Intent('listFolders') fetcher: BearerFetch
    @Intent('searchFile') searcher: BearerFetch
    @Prop() getSelectedFile
    @Prop() closeModal
    @State() selectedFile = null
    @State() selectedFolder = 'root'
    @State() lastFolder = 'root'
    @State() list
    @State() loading = true

    async componentDidLoad() {
        this.list = await this.getFiles('root')
        this.loading = false
    }

    selectId = (data) => {
        this.selectedFile = data
    }

    downloadFile = () => {
        this.getSelectedFile(this.selectedFile)
        this.closeModal()
    }


    getFiles = async (id): Promise<any> => {
        this.loading = true
        const data = await this.fetcher({id})
        return data.data
    }

    getSearchedFiles = async (phrase): Promise<any> => {
        this.loading = true
        const data = await this.searcher({phrase: phrase.value})
        return data.data
    }

    selectFolder = async (id: string) => {
        this.lastFolder = this.selectedFolder
        this.selectedFolder = id
        this.list = await this.getFiles(id)
        this.loading = false
    }

    search = async (value) => {
        this.list = await this.getSearchedFiles({value})
        this.loading = false
        this.selectedFolder = ''
        this.lastFolder = 'root'
    }

    render() {
        if (this.loading) return <div>loading...</div>
        return <div>
            <div>
                {this.selectedFolder !== 'root' &&
                <i onClick={() => this.selectFolder(this.lastFolder)} class="material-icons">arrow_back_ios</i>}
                <h3>Select Files</h3>
            </div>
            <search-bar search={this.search}/>
            {this.list.map(file => <list-item file={file} selectId={this.selectId} selectFolder={this.selectFolder}/>)}
            <div style={{textAlign: 'right'}}>
                <button class="btn btn-green" onClick={this.downloadFile}>
                    Select
                </button>
            </div>

        </div>
    }
}