import {Component, Prop} from '@bearer/core'

@Component({
    tag: 'popin-modal',
    styleUrl: 'modal.css',
    shadow: false
})
export class PopinModal {
    @Prop() closeModal
    render() {
        return (
            <div class="modal">
                <i class="material-icons close-modal" onClick={this.closeModal}>
                    close
                </i>
                <shadow-box styles={{position: 'absolute', padding: '15px'}}>
                    <div class="body">
                        <slot/>
                    </div>
                </shadow-box>
            </div>
        )
    }
}